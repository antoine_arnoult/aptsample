package com.aat.aptsample;

import android.app.Activity;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

@EActivity(R.layout.main_activity)
public class MainActivity extends Activity {

    @AfterViews
    protected void setAfterViews() {
        Toast.makeText(this, "It works !", Toast.LENGTH_SHORT).show();
    }

}
